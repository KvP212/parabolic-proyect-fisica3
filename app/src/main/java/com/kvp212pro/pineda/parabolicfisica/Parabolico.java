package com.kvp212pro.pineda.parabolicfisica;

/**
 * Created by pineda on 2/07/17.
 */
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class Parabolico extends Fragment {

    double x,x1,x2,y1,y2,m,angCanon,angTabla,gtabla,nAng,angbeta,vo,vf,vn,a,b,c,e,totalX,totalY;

    EditText editTextV0, editTextAngTabla, editTextAngCanon;
    ImageButton buttonCalcular;
    TextView textViewTotal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.parabolico, container, false);

        editTextV0 = rootView.findViewById(R.id.editTextV0);
        editTextAngTabla = rootView.findViewById(R.id.editTextAngTabla);
        editTextAngCanon = rootView.findViewById(R.id.editTextAngCanon);

        buttonCalcular = rootView.findViewById(R.id.buttonCalcular);
        textViewTotal = rootView.findViewById(R.id.textViewTotal);

        totalX = 0.00;

        buttonCalcular.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                try {
                    vo = Double.parseDouble(editTextV0.getText().toString());
                    angTabla = Double.parseDouble(editTextAngTabla.getText().toString());
                    angCanon = Double.parseDouble(editTextAngCanon.getText().toString());

                    totalX = (Math.pow(vo,2) * Math.sin( Math.toRadians( angCanon * 2.00 ))) / (9.81 * Math.sin(Math.toRadians(angTabla)));
                    textViewTotal.setText(String.valueOf(String.format("%.2f",totalX)) + "m" );
                    textViewTotal.setVisibility(View.VISIBLE);

                }catch (NumberFormatException e){
                    vo = 0.00;
                    angTabla = 0.00;
                    angCanon = 0.00;
                }
            }
        });

        return rootView;
    }
}
