package com.kvp212pro.pineda.parabolicfisica;

/**
 * Created by pineda on 2/07/17.
 */

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class ParabolicoRecta extends Fragment {

    double x,x1,x2,y1,y2,m,angCanon,angTabla,gtabla,nAng,angbeta,vo,vf,vn,a,b,c,e,totalX,totalY;

    EditText editTextV0, editTextAngTabla, editTextAngCanon, editTextX1, editTextY1, editTextX2, editTextY2;

    ImageButton buttonCalcular;
    TextView textViewTotal;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.parabolico_recta, container, false);

        editTextX1 = rootView.findViewById(R.id.editTextX1);
        editTextY1 = rootView.findViewById(R.id.editTextY1);
        editTextX2 = rootView.findViewById(R.id.editTextX2);
        editTextY2 = rootView.findViewById(R.id.editTextY2);
        editTextV0 = rootView.findViewById(R.id.editTextV0);
        editTextAngTabla = rootView.findViewById(R.id.editTextAngTabla);
        editTextAngCanon = rootView.findViewById(R.id.editTextAngCanon);

        buttonCalcular = rootView.findViewById(R.id.buttonCalcular);
        textViewTotal = rootView.findViewById(R.id.textViewTotal);

        totalX = 0.00;

        buttonCalcular.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                try {
                    x1 = Double.parseDouble(editTextX1.getText().toString());
                    y1 = Double.parseDouble(editTextY1.getText().toString());
                    x2 = Double.parseDouble(editTextX2.getText().toString());
                    y2 = Double.parseDouble(editTextY2.getText().toString());
                    m= (y2-y1)/(x2-x1);
                    vo = Double.parseDouble(editTextV0.getText().toString());
                    angTabla = Double.parseDouble(editTextAngTabla.getText().toString());
                    gtabla = 9.81 * Math.sin(Math.toRadians(angTabla));
                    angCanon = Double.parseDouble(editTextAngCanon.getText().toString());

                    a= (gtabla)/(2 * Math.pow(vo,2) * Math.pow(Math.cos(Math.toRadians(angCanon)),2) );
                    System.out.println("a: " +a);


                    b= (m - Math.tan(Math.toRadians(angCanon)));
                    System.out.println("m: " +m);
                    System.out.println("tan: " + Math.tan(Math.toRadians(angCanon)));
                    System.out.println("b: " +b);

                    c= (-m * x1);
                    System.out.println("c: "+c);

                    totalX = (-b + Math.sqrt( Math.pow((b*-1),2) - (4*a*c)) )/(2 * a);

                    textViewTotal.setText(String.valueOf(String.format("%.2f",totalX)) + "m" );
                    textViewTotal.setVisibility(View.VISIBLE);

                }catch (NumberFormatException e){
                    vo = 0.00;
                    angTabla = 0.00;
                    angCanon = 0.00;
                }
            }
        });


        return rootView;
    }
}
